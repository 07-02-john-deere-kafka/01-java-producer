package com.classpath.producer;

import com.classpath.config.AppConfig;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.IntegerSerializer;
import org.apache.kafka.common.serialization.StringSerializer;
import java.util.Properties;

import static java.util.stream.IntStream.range;

public class MessageProducer {

    public static void main(String[] args) {
        //producer configuration
        Properties properties = new Properties();
        properties.put(ProducerConfig.CLIENT_ID_CONFIG, AppConfig.CLIENT_ID);
        properties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, AppConfig.BOOTSTRAP_SERVERS);
        properties.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, IntegerSerializer.class);
        properties.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class);

        KafkaProducer<Integer, String> kafkaProducer = new KafkaProducer<>(properties);

        range(1, 10).forEach(value -> {
            ProducerRecord<Integer, String> messageRecord = new ProducerRecord(AppConfig.TOPIC_NAME, "Message::"+value);
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            kafkaProducer.send(messageRecord);
        });

        kafkaProducer.close();
        System.out.println(" Message sending completed::");

    }
}
